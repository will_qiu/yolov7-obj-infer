# Inference using yolov7-object-detection of onnx-cpu
This is code is inference onnx model for yolov7-object-detection. We provide cpp and python version. (The nms is exist in model)

## C++

- [Tutorial](cpp/README.md)

## Python

- [Tutorial](python/README.md)