cmake_minimum_required(VERSION 3.0)

set(PrjName inferyolo)
project( ${PrjName} ) 
set(CMAKE_CXX_STANDARD 17)

# Add head directories
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)

# Add library
add_library(${PrjName} SHARED
    package-obj-yolov7.cpp
)

# OpenCV
set(OpenCV_DIR "D:/workspace/opencv/build")
find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} )
target_link_libraries( ${PrjName} PRIVATE ${OpenCV_LIBS} -lstdc++fs )

# gflags
set(GFLAGS_ROOT "D:/workspace/gflags_output")
include_directories(${GFLAGS_ROOT}/include)
target_link_libraries(${PrjName} PRIVATE ${GFLAGS_ROOT}/lib/Release/gflags.lib)

# OnnxRuntime
set(ONNXRUNTIME_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/externals/onnxruntime-win-x64-1.15.1")
include_directories(${ONNXRUNTIME_ROOT}/include)
target_link_libraries(${PrjName} PRIVATE ${ONNXRUNTIME_ROOT}/lib/onnxruntime.lib)

# Build setting
set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON)
set_target_properties(${PrjName} PROPERTIES OUTPUT_NAME "inferyolo")
set_target_properties(${PrjName} PROPERTIES

    ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin
    LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin
    RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin
)