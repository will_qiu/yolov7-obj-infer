#include <iostream>
#include <chrono>
#include <vector>
#include <stdio.h>
#include <float.h>
#include <gflags/gflags.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include "slog.hpp"
#include "utils.hpp"
#include "infer_on.hpp"

static const char help_message[] = "Print a usage message.";
static const char input_message[] = "Required. An input to process. The input must be a single image, a folder of "
    "images, video file or camera id.";
static const char model_message[] = "Required. Path to an .xml file with a trained model.";
static const char thresh_output_message[] = "Optional. Probability threshold for detections.";
static const char using_nms_message[] = "Optional. Using nsm process for detections.";

#define DEFINE_INPUT_FLAGS
DEFINE_string(i, "", input_message); 
DEFINE_bool(h, false, help_message);
DEFINE_string(m, "", model_message);
DEFINE_double(t, 0.5, thresh_output_message);
DEFINE_bool(nms, false, help_message);

/**
 * \brief This function shows a help message
 */
static void showUsage() {
    std::cout << std::endl;
    std::cout << "object_detection_onnx [OPTION]" << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << std::endl;
    std::cout << "    -h                        " << help_message << std::endl;
    std::cout << "    -i                        " << input_message << std::endl;
    std::cout << "    -m \"<path>\"             " << model_message << std::endl;
    std::cout << "    -t                        " << thresh_output_message << std::endl;
    std::cout << "    -nms                      " << using_nms_message << std::endl;
}

bool ParseAndCheckCommandLine(int argc, char* argv[]) {
    // ---------------------------Parsing and validation of input args--------------------------------------
    gflags::ParseCommandLineNonHelpFlags(&argc, &argv, true);
    if (FLAGS_h) {
        showUsage();
        return false;
    }

    if (FLAGS_i.empty()) {
        throw std::logic_error("Parameter -i is not set");
    }

    if (FLAGS_m.empty()) {
        throw std::logic_error("Parameter -m is not set");
    }

    return true;
}

int main(int argc, char** argv){
    try {

        // ------------------------------ Parsing and validation of input args ---------------------------------
        if (!ParseAndCheckCommandLine(argc, argv)) {
            return 0;
        }

        // FPS variables
        double totalTime;
        double fps = 0.0;

        // Classes
        std::string folderPath = getFolderPath(FLAGS_m);
        std::vector<std::string> classesPath = findTxtFilesInFolder(folderPath);
        std::vector<std::string> className = readTxtFile(classesPath[0]);
        
        // BOX
        std::vector<BoxInfo> generatedBoxes;
        // Color palette
        ColorPalette palette(className.size() > 0 ? className.size() : 100);
        // Model
        NetConfig config = { FLAGS_nms, static_cast<float>(FLAGS_t), 0.5, FLAGS_m, className};
        YOLO net(config);

        bool isImagePath = isImage(FLAGS_i);
        if (isImagePath){
            // Image
            cv::Mat frame = cv::imread(FLAGS_i);
            if (frame.empty()) {
                slog::err << "Error: Unable to load image." << slog::endl;
                return 1;
            }
            auto startTime = std::chrono::high_resolution_clock::now();
            // Detect
            generatedBoxes = net.detect(frame);
            auto detecTime = std::chrono::high_resolution_clock::now();
            CountTotalTime(totalTime, startTime, detecTime, "detection time");
            // Draw
            net.drawBoxes(frame, generatedBoxes, palette);
            auto drawTime = std::chrono::high_resolution_clock::now();
            CountTotalTime(totalTime, startTime, drawTime, "draw time");
            fps = 1.0 / totalTime;
            slog::info << "fps:" << fps << slog::endl;
            // Show
            static const std::string windowName = "Detection";
            cv::namedWindow(windowName, cv::WINDOW_NORMAL);
            cv::imshow(windowName, frame);
            // Quit
            while (true) {
                int key = cv::waitKey(0);
                // Check if 'q', 'Q', or ESC key was pressed
                if (key == 'q' || key == 'Q' || key == 27) {
                    cv::destroyAllWindows();
                    break;
                }
            }
        }
        else{
            
            // Video or webcam
            cv::VideoCapture cap(FLAGS_i);
            if (!cap.isOpened()) {
                slog::err << "Error: Unable to open video file." << slog::endl;
                return 1;
            }

            cv::Mat frame;
            while (cap.read(frame)) {
                auto startTime = std::chrono::high_resolution_clock::now();
                // Detect
                generatedBoxes = net.detect(frame);
                auto detecTime = std::chrono::high_resolution_clock::now();
                CountTotalTime(totalTime, startTime, detecTime, "detection time");

                // Draw
                net.drawBoxes(frame, generatedBoxes, palette);
                auto drawTime = std::chrono::high_resolution_clock::now();
                CountTotalTime(totalTime, detecTime, drawTime, "draw time");
                CountTotalTime(totalTime, startTime, drawTime, "all time");
                fps = 1.0 / totalTime;
                slog::info << "fps:" << fps << slog::endl;

                // Show
                static const std::string windowName = "Detection";
                cv::namedWindow(windowName, cv::WINDOW_NORMAL);
                cv::imshow(windowName, frame);
                // Quit
                int key = cv::waitKey(1);
                // Check if 'q', 'Q', or ESC key was pressed
                if (key == 'q' || key == 'Q' || key == 27) { 
                    break;
                }
            }

            cv::destroyAllWindows();
            return 0;
        }
    } catch (const std::exception& error) {
        slog::err << error.what() << slog::endl;
        return 1;
    } catch (...) {
        slog::err << "Unknown/internal exception happened." << slog::endl;
        return 1;
    }

    return 0;
}