#include <fstream>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/dnn/dnn.hpp>
#include <opencv2/bioinspired.hpp>
#include <sstream>

struct NetConfig
{
	float confThreshold;
	float nmsThreshold; 
	std::string modelPath;
    std::vector<std::string> Classes;
	int inpWidth;
	int inpHeight;
};

class YOLO
{
public:
	YOLO(NetConfig config);
	void detect(cv::Mat& frame);
private:
	int inpWidth;
	int inpHeight;
	std::vector<std::string> ClassNames;
	int numClass;

	float confThreshold;
	float nmsThreshold;
	cv::dnn::Net net;
};

YOLO::YOLO(NetConfig config)
{
    // Setting Threshold
	this->confThreshold = config.confThreshold;
	this->nmsThreshold = config.nmsThreshold;

    // Loading model
	this->net = cv::dnn::readNet(config.modelPath);

    // Setting parameter
	this->inpHeight = config.inpHeight;
	this->inpWidth = config.inpWidth;
    this->ClassNames = config.Classes;
	this->numClass = ClassNames.size();
}