onnxruntime<1.16.0
numpy
opencv-python>=4.1.1
torch>=1.7.0,!=1.12.0
torchvision>=0.8.1,!=0.13.0