import numpy as np
from .bbox import letterbox, non_max_suppression
from typing import List, Tuple, Dict
import torch
import onnxruntime as rt

AOLLOW_FORMAT = {
                    "images": [".jpg", ".bmp", ".png", ".jpeg", ".JPG", ".JPEG", ".PNG", ".BMP"],
                    "annotation": [".txt", ".xml", ".json"]
                }

def preprocess_image(img0: np.ndarray, new_shape):
    """
    Preprocess image according to YOLOv7 input requirements. 
    Takes image in np.array format, resizes it to specific size using letterbox resize, converts color space from BGR (default in OpenCV) to RGB and changes data layout from HWC to CHW.
    
    Parameters:
      img0 (np.ndarray): image for preprocessing
    Returns:
      img (np.ndarray): image after preprocessing
      img0 (np.ndarray): original image
    """
    # resize
    img = letterbox(img0, new_shape, auto=False)[0]
    
    # Convert
    img = img.transpose(2, 0, 1)
    img = np.ascontiguousarray(img)
    return img, img0

def prepare_input_tensor(image: np.ndarray):
    """
    Converts preprocessed image to tensor format according to YOLOv7 input requirements. 
    Takes image in np.array format with unit8 data in [0, 255] range and converts it to torch.Tensor object with float data in [0, 1] range
    
    Parameters:
      image (np.ndarray): image for conversion to tensor
    Returns:
      input_tensor (torch.Tensor): float tensor ready to use for YOLOv7 inference
    """
    input_tensor = image.astype(np.float32)  # uint8 to fp16/32
    input_tensor /= 255.0  # 0 - 255 to 0.0 - 1.0
    
    if input_tensor.ndim == 3:
        input_tensor = np.expand_dims(input_tensor, 0)
    return input_tensor

class YOLO:
    def __init__(self, model_path:str):
      self.sess = rt.InferenceSession(model_path)
      self.sess_input = self.sess.get_inputs()[0].name
      self.sess_output = self.sess.get_outputs()[0].name
      self.input_shape = self.sess.get_inputs()[0].shape

      # Warm UP
      print("Warmup!!!")
      input_data = np.random.rand(self.input_shape[0], self.input_shape[1], \
                                  self.input_shape[2], self.input_shape[3]).astype(np.float32)
      num_warmup = 10
      for i in range(num_warmup):
          self.sess.run(None, {self.sess_input: input_data})

    def detect(self, img, conf_thres: float = 0.25, iou_thres: float = 0.45, classes: List[int] = None, agnostic_nms: bool = False, nms=False):
        # print("Model input shape:{}".format(input_shape))
        preprocessed_img, orig_img = preprocess_image(img, [self.input_shape[2], self.input_shape[3]])
        input_tensor = prepare_input_tensor(preprocessed_img)
        if nms:
            predictions = torch.from_numpy(self.sess.run([self.sess_output], {self.sess_input: input_tensor})[0])
            pred = non_max_suppression(predictions, conf_thres, iou_thres, classes=classes, agnostic=agnostic_nms)
        else:
            # Collect threshold detection
            predictions = self.sess.run([self.sess_output], {self.sess_input: input_tensor})[0]
            pred = torch.from_numpy(np.array(([  [detec[1], detec[2], detec[3], detec[4], detec[-1], detec[-2]]  for detec in predictions if detec[-1] > conf_thres ])))
            # print(pred)
        return pred, orig_img, input_tensor.shape