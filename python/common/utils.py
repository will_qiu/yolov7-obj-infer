import numpy as np
import os

AOLLOW_FORMAT = {
                    "image": [".jpg", ".bmp", ".png", ".jpeg", ".JPG", ".JPEG", ".PNG", ".BMP"],
                    "annotation": [".txt", ".xml", ".json"],
                    "video": [".mp4", ".avi", ".mov", ".mkv", ".MP4", ".AVI", ".MOV", ".MKV"]
                }

def get_color_palette(class_names):
    # Colors for visualization
    colors = {name: [np.random.randint(0, 255) for _ in range(3)]
            for i, name in enumerate(class_names)}

    return colors

def get_classes_list(classes_path:str):
    return [cls for cls in read_txt(classes_path).split("\n") if cls !=""]

def read_txt(path:str):
    with open(path) as f:
        return f.read()
    
def abs_path(path:str):
    return path if os.path.isabs(path) else os.path.abspath(path)