from argparse import ArgumentParser, SUPPRESS
from common import AOLLOW_FORMAT, YOLO, draw_boxes, get_classes_list, get_color_palette, abs_path
import os, sys, cv2, glob, time

def build_argparser():
    parser = ArgumentParser(add_help=False)
    args = parser.add_argument_group('Options')
    args.add_argument('-h', '--help', action='help', default=SUPPRESS, help='Show this help message and exit.')
    args.add_argument('-m', '--model', required=True, help = "Model path")
    args.add_argument('-i', '--input', required=True, help = "Source path")
    args.add_argument('-t', '--threshold', required=True, help = "Threshold: 0.0~1.0")
    return parser

def display_image(frame):
    cv2.namedWindow('Detection', 0)
    cv2.imshow("Detection", frame)
    key = cv2.waitKey(0)
    ESC_KEY = 27
    # Quit.
    if  key in {ord('q'), ord('Q'), ESC_KEY}:
        cv2.destroyAllWindows()

def save_images(frame):
    global SAVE_KEY
    cv2.imwrite(f"result_{SAVE_KEY}.jpg", frame)
    SAVE_KEY += 1
    
def process_image(file_path, obj, threshold, NAMES, COLORS, display=True, save=False):
    image = cv2.imread(file_path)
    if image is not None:
        print(f"Displaying image: {file_path}")
        image = infer_action(obj, image, threshold, NAMES, COLORS)
        if save:
            save_images(image)
            
        if display:
            display_image(image)
    else:
        print(f"Could not read the image: {file_path}")

def process_video(file_path, obj, threshold, NAMES, COLORS, display=True, save=False):
    if file_path.isnumeric():
        file_path = int(file_path)
    else:
        file_path = file_path
    cap = cv2.VideoCapture(file_path)
    if not cap.isOpened():
        print(f"Could not open video: {file_path}")
        return

    while True:
        ret, frame = cap.read()
        if not ret:
            break
        frame = infer_action(obj, frame, threshold, NAMES, COLORS)
        if save:
            save_images(frame)

        if display:
            cv2.namedWindow('Detection', 0)
            cv2.imshow('Detection', frame)
            key = cv2.waitKey(1)
            ESC_KEY = 27
            # Quit.
            if  key in {ord('q'), ord('Q'), ESC_KEY}:
                break
        
    cap.release()
    cv2.destroyAllWindows()   

def process_directory(directory_path, obj, threshold, NAMES, COLORS, display=True, save=False):
    for root, _, files in os.walk(directory_path):
        for file in files:
            file_path = os.path.join(root, file)
            if file_path.lower().endswith(tuple(AOLLOW_FORMAT['image'])):
                process_image(file_path, obj, threshold, NAMES, COLORS, display, save)

def infer_action(obj, frame, threshold, NAMES, COLORS):
    # Obj-detection
    obj_start = time.time()
    pred, orig_img, input_shape =  obj.detect(frame, float(threshold))
    obj_detec_time = time.time() - obj_start
    print((f"Obj-detection time: { obj_detec_time } s"))
    
    # Draw
    draw_start = time.time()
    frame = draw_boxes(pred, input_shape, orig_img, NAMES, COLORS)
    end = time.time()
    draw_time = end - draw_start
    print(f"Drawing time: {draw_time} s")
    
    # FPS
    all_time = end - obj_start
    print(f"All time: {all_time} s")
    print("All FPS: {}".format( round(1/all_time, 3) ))
    print("-"*50)
    
    return frame

def main(args):
    # Abs process
    args.model = abs_path(args.model)
    args.input = abs_path(args.input)
    
    # Classes / Loading model
    classes_path = glob.glob( os.path.join(os.path.split(args.model)[0], "*.txt" ) )
    classes_path = classes_path[0] if len(classes_path) > 0 else ""
    NAMES = get_classes_list(classes_path) if os.path.exists(classes_path) else [str(i) for i in range(1000)]
    COLORS = get_color_palette(NAMES)
    print(f"Loading object detection model...")
    obj = YOLO(args.model)
    
    if os.path.isfile(args.input):
        if args.input.lower().endswith(tuple(AOLLOW_FORMAT['image'])):
            process_image(args.input, obj, args.threshold, NAMES, COLORS)
        elif args.input.lower().endswith(tuple(AOLLOW_FORMAT['video'])):
            process_video(args.input, obj, args.threshold, NAMES, COLORS)
        else:
            print("Unsupported file type.")
    elif os.path.isdir(args.input):
        process_directory(args.input, obj, args.threshold, NAMES, COLORS)
    else:
        print("Invalid input path.")

if __name__ == '__main__':
    args = build_argparser().parse_args()
    sys.exit(main(args) or 0)